const PLANNER = new function() {
    var that = this;
    console.log("THIS = " + this);

    this.init = function() {

	proj4.defs('EPSG:2180', "+proj=tmerc +lat_0=0 +lon_0=19 +k=0.9993 +x_0=500000 +y_0=-5300000 +ellps=GRS80 +units=m +no_defs");
	
    // OSM 
    
	const osmLayer = new ol.layer.Tile({source: new ol.source.OSM()});

    // OPEN TOPO

	const openTopoSource = new ol.source.XYZ({
        url: "https://opentopomap.org/{z}/{x}/{y}.png",
	    opaque : true
	});
	const openTopoLayer = new ol.layer.Tile({source: openTopoSource});
	
    
    // STRAVA 
    const stravaSource = new ol.source.XYZ({
        url: "https://heatmap-external-a.strava.com/tiles-auth/all/gray/{z}/{x}/{y}.png",
	    opaque : true
	});
	const stravaLayer = new ol.layer.Tile({source: stravaSource});
    
    
	// that.osmLayer openmap
	that.source = new ol.source.Vector();
	
	// feature z planowana trasa
	that.vector = new ol.layer.Vector({
	    source: that.source,
	    style: new ol.style.Style({
		stroke: new ol.style.Stroke({
		    color: '#0000FF',
		    width: 2
		})
	    })
	});
	
	// cien innej trasy do porownania
	that.loaded_gpx_shadow = new ol.layer.Vector({
	    name: 'gpx_shadow_vectorlayer',
	    source: new ol.source.Vector(),
	    style: new ol.style.Style({
		stroke: new ol.style.Stroke({
		    color: '#ff1111',
		    width: 2
		})
	    })
	});
	
	
	var draw;

		
	 that.map = new ol.Map({
	     layers: [osmLayer, openTopoLayer, stravaLayer, that.vector, that.loaded_gpx_shadow],
	    controls: [],
	    target: 'map',
	    view: new ol.View({
		projection: 'EPSG:2180',
		center: [359615.7542, 505522.0951],
		zoom: 7
	    })
	});
	
	var modify = new ol.interaction.Modify({source: that.source});
	that.map.addInteraction(modify);

	
	removeInteractions();
	addInteractions();

	// disable zoom on Openlayer map
	that.map.getInteractions().forEach(
	    function(interaction) {
		if (interaction instanceof ol.interaction.MouseWheelZoom) {
		    interaction.setActive(false);
		}
	    },
	    document);

	
	var osm_opacitySlider = document.getElementById("osm-opacitySlider");
	osm_opacitySlider.oninput = function() {
	    osmLayer.setOpacity(this.value/100);
	}

	var openTopo_opacitySlider = document.getElementById("openTopo-opacitySlider");
	openTopo_opacitySlider.oninput = function() {
	    openTopoLayer.setOpacity(this.value/100);
	}
	
    var strava_opacitySlider = document.getElementById("strava-opacitySlider");
	strava_opacitySlider.oninput = function() {
	    stravaLayer.setOpacity(this.value/100);
	}
    
	osmLayer.setOpacity(osm_opacitySlider.value/100);
	openTopoLayer.setOpacity(openTopo_opacitySlider.value/100);
    stravaLayer.setOpacity(strava_opacitySlider.value/100);

    }

    
    
    function addInteractions() {
	draw = new ol.interaction.Draw({
            source: that.source,
            type: "LineString"
	});

	draw.on('drawstart', function (event) {
	    event.feature.on('change', function (eventt) {
		document.getElementById('lengthcontainer').textContent = Math.round(eventt.target.getGeometry().getLength()) + "m" ;
	    });
	});

	// usuwamy mozliwosc tworzenia wielu featuresow - czyli jak sie zakonczy rysowanie jednego to wiecej nie zrobisz
	draw.on('drawend', function (event) {
	    that.map.removeInteraction(draw);
	});

	that.map.addInteraction(draw);
    }





    // disable map draging OpenLayer
    function removeInteractions() {
	var interactions = that.map.getInteractions();
	interactions.forEach( function(i) { that.map.removeInteraction(i);});
    }


    // przelacza eventy na GEOPORTAL
    this.mouseGEO = function() {
	document.getElementById("map").style.pointerEvents = 'none';
    }

    // przelacza eventy na OLMap
    this.mouseOLMap = function() {
	document.getElementById("map").style.pointerEvents = 'all';
    }

    // load gpx source file
    this.loadGPXSource = function(event) {
	that.vector.getSource().clear();
	var gpxFormat = new ol.format.GPX();
	f = event.target.files[0];
	var reader = new FileReader();
	reader.readAsText(f,"UTF-8");
	reader.onload = function (evt) {
	    var gpxFeatures = gpxFormat.readFeatures(evt.target.result,{
		dataProjection:'EPSG:4326',
		featureProjection:'EPSG:2180'
	    });
	    that.vector.getSource().addFeatures(gpxFeatures);
	    document.getElementById("lengthcontainer").textContent = Math.round(ol.Sphere.getLength(that.vector.getSource().getFeatures()[0].getGeometry())) + "m";
	    that.vector.getSource().getFeatures()[0].on('change', function(eventt) {
		document.getElementById("lengthcontainer").textContent = Math.round(ol.Sphere.getLength(eventt.target.getGeometry())) + "m";
	    });
	}
	that.map.removeInteraction(draw);
    }

    // load gpx shadow files
    this.loadGPXShadowFiles = function (evt) {
	// files is a FileList of File objects. List some properties.
	var gpxFormat = new ol.format.GPX();
	var gpxFeatures;
	var files = evt.target.files;
	for (var i = 0, f; f = files[i]; i++) {
	    var reader = new FileReader();
	    reader.readAsText(files[i], "UTF-8");
	    reader.onload = function (evt) {
		gpxFeatures = gpxFormat.readFeatures(evt.target.result,{
		    dataProjection:'EPSG:4326',
		    featureProjection:'EPSG:2180'
		});
		that.loaded_gpx_shadow.getSource().addFeatures(gpxFeatures);
	    }
	}
    }

    // przygotowuje i downloaduje gpx'a
    this.downloadGPX = function(filename) {
	var path = that.vector.getSource().getFeatures();
	var gpxFormat = new ol.format.GPX();
	var gpxString = gpxFormat.writeFeatures(that.vector.getSource().getFeatures(),{
	    dataProjection:'EPSG:4326',
	    featureProjection:'EPSG:2180'
	});
	download(filename, gpxString);
	//download(filename, myGPXGenerator(path[0].getGeometry().getCoordinates()));
    }

    // downloaduje plik
    function download(filename, text) {
	var element = document.createElement('a');
	element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
	element.setAttribute('download', filename);

	element.style.display = 'none';
	document.body.appendChild(element);

	element.click();

	document.body.removeChild(element);
    }

    // ustawia na GEO ten sam extent ktory jest obecnie na OpenLayer
    this.moveGEO2OLMap = function() {
	var srcExtent = that.map.getView().calculateExtent(that.map.getSize());
	ILITEAPI.zoomTo({
            "xmin": srcExtent[0],
            "ymin": srcExtent[1],
            "xmax": srcExtent[2],
            "ymax": srcExtent[3],
            "spatialReference": {
		"wkid": 2180
            }
	});
    }

    // wyrownuje extety olmap do geo
    this.moveOLMap2GEO = function() {
	console.log("moveOLMap2GEO");
	ILITEAPI.getExtent(
	    function(extent) {
		var ee = [extent.xmin, extent.ymin , extent.xmax , extent.ymax];
		that.map.getView().fit(ee,{nearest: false,constrainResolution: false});
	    });
    }


    ////////////////////////////////////////////////////////////////////////////////
    // garbage

    // wyrzuca na konsole extenty i center dla GEO i Openmap
    function compareExtents() {
	console.log("-----------------------------------------------------------------------------------------------------------------");
	console.log("-----------------------------------------------------------------------------------------------------------------");
	console.log("-----------------------------------------------------------------------------------------------------------------");

	console.log("openlayers extent: " + that.map.getView().calculateExtent(that.map.getSize()));
	console.log("openlayers center: " + that.map.getView().getCenter());

	console.log("-----------------------------------------------------------------------------------------------------------------");

	ILITEAPI.getExtent(
	    function (extent) {
		console.log(" GEO extent: " + extent.xmin + ", " + extent.ymin + ", " + extent.xmax + ", " + extent.ymax + " :: epsg:" + extent.spatialReference.wkid);
	    }
	);
	ILITEAPI.getCenterAndScale(
	    function (center) {
		console.log(" GEO Center: " + center.center);
	    }
	);
    }
}


$(PLANNER.init);
